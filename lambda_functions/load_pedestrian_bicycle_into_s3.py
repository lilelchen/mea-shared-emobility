import json
import os
import boto3
import requests
from datetime import datetime

#initalize boto3 with s3
s3 = boto3.client('s3')
S3_BUCKET = "pedestrianbicycles3"

def get_api_json():
    url = 'https://data.stadt-zuerich.ch/api/3/action/datastore_search?resource_id=9a9f9221-d6a0-4289-9962-410557adef93'
    # url = 'https://data.stadt-zuerich.ch/api/3/action/datastore_search?resource_id=9a9f9221-d6a0-4289-9962-410557adef93&limit=5'

    response_API = requests.get(url)
    #print(response_API.status_code)
    data = response_API.text
    data = json.loads(data)
    return data


def write_to_s3(json_object, timestamp):
    s3.put_object(
        Body=json.dumps(json_object),
        Bucket=S3_BUCKET,
        Key= 'pedestrianbicycles3_zurich.json')
    return None
    
def lambda_handler(event, context):
    now_time = datetime.now().strftime("%Y%m%d-%H%M%S")
    result = get_api_json()
    write_to_s3(result, now_time)

    print('Put Complete')
    
    return {
        'statusCode': 200,
        'body': json.dumps('Works!')
    }
