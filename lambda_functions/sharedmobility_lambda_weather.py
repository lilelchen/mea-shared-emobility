#----------------------
# Availability of Shared Mobility Services in the City of Zurich
# 
# sharedmobility_lambda_weather.py
# Authors: Jonas Klinkert, Manuel Frei, Anna-Lena Klaus
# Date: 24.04.2022
#
#----------------------

import json
import os
import requests
import pandas as pd
import psycopg2
from datetime import datetime

# API input constants
LOC_MAINSTATION = {"coord_x": "8.53990", "coord_y": "47.37722"}
LOC_OERLIKON = {"coord_x": "8.54411", "coord_y": "47.41147"}
API_KEY = os.environ["API_KEY"]

# SQL login constants
DB_LOGIN = {
    "db_host": os.environ["DB_HOST"],
    "db_name": os.environ["DB_NAME"],
    "db_user": os.environ["DB_USER"],
    "db_pw": os.environ["DB_PW"]}

# SQL column names in table
column_names_table = ["coord", "weather", "base", "main", "visibility", "wind", "clouds", "rain", "snow", "dt", "sys",
                      "timezone", "id", "name", "cod"]


# create connection to database
def set_connection_db(db_host, db_name, db_user, db_pw):
    conn = psycopg2.connect("host={} dbname={} user={} password={}".format(db_host, db_name, db_user, db_pw))
    cur = conn.cursor()
    conn.set_session(autocommit=True)
    return cur


# fetch data from API
def fetch_data(coord_x, coord_y, api_key):
    base_url = f"https://api.openweathermap.org/data/2.5/weather?lat={coord_y}&lon={coord_x}&appid={api_key}&units=metric"
    response = requests.get(base_url)
    result_json = json.loads(response.text)
    return result_json


# extract data from API response
def extract_data(json_response, timestamp):
    result_list = []
    for name in column_names_table:
        if name in json_response:
            node_string = json.dumps(json_response[f"{name}"], ensure_ascii=False)
            result_list.append(node_string)
        else:
            node_string = "None"
            result_list.append(node_string)
    result_list.append(timestamp)
    return result_list


# load extracted data into database
def load_data(data, cursor):
    columns_string = "coord, weather, base, main, visibility, wind, clouds, rain, snow, dt, sys, timezone, id, name, cod, time_gmt"
    cursor.execute(f"INSERT INTO sharedmobility_weather ({columns_string}) \
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", \
                   (data))
    return "Load successful"


def lambda_handler(event, context):
    # get execution timestamp
    timestamp = datetime.now()

    # connect to database
    cursor = set_connection_db(**DB_LOGIN)

    # create table in database to store retrieved data
    cursor.execute(
        "CREATE TABLE IF NOT EXISTS sharedmobility_weather (coord text, weather text, base text, main text, visibility text, wind text, clouds text, rain text, snow text, dt text, sys text, timezone text, id text, name text, cod text, time_gmt timestamp);")

    # query and upload mainstation data
    response_mainstation = fetch_data(**LOC_MAINSTATION, api_key=API_KEY)
    data_mainstation = extract_data(response_mainstation, timestamp=timestamp)
    load_data(data_mainstation, cursor=cursor)

    # query and upload oerlikon data
    response_oerlikon = fetch_data(**LOC_OERLIKON, api_key=API_KEY)
    data_oerlikon = extract_data(response_oerlikon, timestamp=timestamp)
    load_data(data_oerlikon, cursor=cursor)

    return {
        'statusCode': 200,
        'body': "SUCCESS"
    }