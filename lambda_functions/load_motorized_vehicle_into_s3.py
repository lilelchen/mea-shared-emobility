# import csv
import boto3
import requests
from datetime import datetime

from botocore.vendored import requests
import csv

# initalize boto3 with s3
s3 = boto3.client('s3')

# downloading the csv file
def lambda_handler(event, context):
    S3_BUCKET = "motorizedverhicles3"
    url = 'https://data.stadt-zuerich.ch/dataset/sid_dav_verkehrszaehlung_miv_od2031/download/sid_dav_verkehrszaehlung_miv_OD2031_2022.csv'
    session = requests.Session()
    raw_data = session.get(url)
    filename = 'motorizedverhicles3_zurich.csv'

    # ensure encoding of csv-file
    decoded_content = raw_data.content.decode('utf-8')
    reader = csv.reader(decoded_content.splitlines(), delimiter=',')

    # loading to s3
    s3.put_object(Bucket=S3_BUCKET, Key=filename, Body=decoded_content)

    print('Put Complete')

    return {
        'statusCode': 200,
        'message': 'success!!'
    }


