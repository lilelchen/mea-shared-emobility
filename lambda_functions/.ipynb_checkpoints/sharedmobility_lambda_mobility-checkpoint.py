import json
import requests
import pandas as pd
import sqlalchemy
import psycopg2
from datetime import datetime

# API input constants
LOC_MAINSTATION = {"location_name": "Mainstation", "coord_x": "8.53990", "coord_y": "47.37722", "tolerance": "500"}
LOC_OERLIKON = {"location_name": "Oerlikon", "coord_x": "8.54411", "coord_y": "47.41147", "tolerance": "500"}
PAGE_ITEMS = 50

# SQL login constants
DB_LOGIN = {
    "db_host": "sharedmob.c5urbx6qvldk.us-east-1.rds.amazonaws.com",
    "db_port": "5432",
    "db_name": "sharedmob",
    "db_user": "postgres",
    "db_pw": "Qwertzuiop1",
}
DB_TABLE = "shared_mobility"


# function to get dataframe from single API call json
def get_api_json(coord_x, coord_y, tolerance, offset):
    base_url=f"https://api.sharedmobility.ch/v1/sharedmobility/identify?Geometry={coord_x},{coord_y}&Tolerance={tolerance}&offset={offset}&geometryFormat=esrijson"
    response = requests.get(base_url)
    result_json = json.loads(response.text)
    result_df = pd.json_normalize(result_json)
    return result_df


# function to get dataframe containing all data (pages) from one location
def get_data(location_name, coord_x, coord_y, tolerance, timestamp):
    call_again = True
    counter=0
    df_list = []
    while call_again:
        df_get = get_api_json(coord_x, coord_y, tolerance, PAGE_ITEMS * counter)
        if len(df_get) > 0:
            df_list.append(df_get)
            counter +=1
        if len(df_get) < 50:
            call_again = False
    if len(df_list) > 1:
        result = pd.concat(df_list, ignore_index=True)
        result["locationName"] = location_name
        result["time"] = timestamp
        return result
    elif len(df_list) > 0:
        result = df_list[0]
        result["location_name"] = location_name
        result["timestamp"] = timestamp
        return result
    else:
        return pd.DataFrame()


# function to establish db connection (engine)
def create_db_engine(db_host, db_port, db_name, db_user, db_pw):
        engine_url = f"postgresql://{db_user}:{db_pw}@{db_host}:{db_port}/{db_name}"
        engine = sqlalchemy.create_engine(engine_url)
        return engine
        




def lambda_handler(event, context):
    
    # Get execution timestamp
    timestamp = datetime.now()
    
    # Connect to DB engine
    engine = create_db_engine(**DB_LOGIN)
    engine.connect()
    
    # query and upload mainstation data
    data_mainstation = get_data(**LOC_MAINSTATION, timestamp=timestamp)
    data_mainstation.to_sql(name=DB_TABLE, con=engine, if_exists="append", index=False)
    
    # query and upload oerlikon data
    data_oerlikon = get_data(**LOC_OERLIKON, timestamp=timestamp)
    data_oerlikon.to_sql(name=DB_TABLE, con=engine, if_exists="append", index=False)
    
    return {
        'statusCode': 200,
        'body': "SUCCESS"
    }
