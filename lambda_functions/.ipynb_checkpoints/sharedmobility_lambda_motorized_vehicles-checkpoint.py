#----------------------
# Availability of Shared Mobility Services in the City of Zurich
# 
# sharedmobility_lambda_motorized_vehicles.py
# Authors: Jonas Klinkert, Manuel Frei, Anna-Lena Klaus
# Date: 24.04.2022
#
#----------------------

import json
import boto3
import requests
from datetime import datetime

#initalize boto3 with s3
s3 = boto3.client('s3')
S3_BUCKET = "motorizedverhicles3"

# defining the API URL
def get_api_json():
    url = 'https://data.stadt-zuerich.ch/api/3/action/datastore_search?resource_id=bc2d7c35-de13-45e9-be21-538d9eab3653&limit=500000'
    # url = 'https://data.stadt-zuerich.ch/api/3/action/datastore_search?resource_id=bc2d7c35-de13-45e9-be21-538d9eab3653&limit=5'

    response_API = requests.get(url)
    #print(response_API.status_code)
    data = response_API.text
    data = json.loads(data)
    return data

# writes data to S3
def write_to_s3(json_object, timestamp):
    s3.put_object(
        Body=json.dumps(json_object),
        Bucket=S3_BUCKET,
        Key= 'motorizedvehicles3_zurich.json')
    return None
    
# lambda handler executes
def lambda_handler(event, context):
    now_time = datetime.now().strftime("%Y%m%d-%H%M%S")
    result = get_api_json()
    write_to_s3(result, now_time)

    print('Put Complete')
    
    return {
        'statusCode': 200,
        'body': json.dumps('Works!')
    }
