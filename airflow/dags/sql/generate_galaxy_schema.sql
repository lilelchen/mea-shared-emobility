-- create dim_location table
CREATE TABLE IF NOT EXISTS dim_location (
	dim_locationID SERIAL PRIMARY KEY,
	location_name VARCHAR
	);
TRUNCATE TABLE dim_location RESTART IDENTITY CASCADE;
WITH distinct_locations AS (
	SELECT location FROM stage_mobility
	UNION
	SELECT location FROM stage_weather
	UNION
	SELECT location FROM stage_traffic
	) INSERT INTO dim_location (location_name)
	SELECT location FROM distinct_locations;
		
		

		
-- create dim_timeperiod table
CREATE TABLE IF NOT EXISTS dim_timeperiod (
	dim_timeperiodID SERIAL PRIMARY KEY,
	date_time TIMESTAMP,
	year INT,
	month INT,
	day INT,
	hour INT,
	weekday INT,
	weekend BOOLEAN
	);
TRUNCATE TABLE dim_timeperiod RESTART IDENTITY CASCADE;
WITH distinct_timeperiods AS (
	SELECT date_time FROM stage_mobility
	UNION
	SELECT date_time FROM stage_weather
	UNION
	SELECT "DATUM" FROM stage_traffic
	) INSERT INTO dim_timeperiod (date_time, year, month, day, hour, weekday, weekend)
		SELECT 
			date_time,
			EXTRACT(YEAR FROM date_time),
			EXTRACT(MONTH FROM date_time),
			EXTRACT(DAY FROM date_time),
			EXTRACT(HOUR FROM date_time),
			EXTRACT(ISODOW FROM date_time),
			CASE WHEN EXTRACT(ISODOW FROM date_time) >= 6 THEN TRUE ELSE FALSE END
		FROM distinct_timeperiods
		ORDER BY date_time;
		

-- create dim_provider table
CREATE TABLE IF NOT EXISTS dim_provider (
	dim_providerID SERIAL PRIMARY KEY,
	provider_name VARCHAR
	);
TRUNCATE TABLE dim_provider RESTART IDENTITY CASCADE;
INSERT INTO dim_provider (provider_name)
	SELECT DISTINCT provider_name FROM stage_mobility;


-- create dim_pickup_type table
CREATE TABLE IF NOT EXISTS dim_pickup_type (
	dim_pickup_typeID SERIAL PRIMARY KEY,
	pickup_type VARCHAR
	);
TRUNCATE TABLE dim_pickup_type RESTART IDENTITY CASCADE;
INSERT INTO dim_pickup_type (pickup_type)
	SELECT DISTINCT pickup_type FROM stage_mobility;


-- create dim_vehicle_type table
CREATE TABLE IF NOT EXISTS dim_vehicle_type (
	dim_vehicle_typeID SERIAL PRIMARY KEY,
	vehicle_type VARCHAR
	);
TRUNCATE TABLE dim_vehicle_type RESTART IDENTITY CASCADE;
INSERT INTO dim_vehicle_type (vehicle_type)
	SELECT DISTINCT vehicle_type FROM stage_mobility;
	
	
-- create dim_weather_condition table
CREATE TABLE IF NOT EXISTS dim_weather_condition (
	dim_weather_conditionID SERIAL PRIMARY KEY,
	weather_category VARCHAR,
	weather_description VARCHAR
	);
TRUNCATE TABLE dim_weather_condition RESTART IDENTITY CASCADE;
WITH distinct_weather AS (
	SELECT DISTINCT weather_category, weather_description
	FROM stage_weather
	) INSERT INTO dim_weather_condition (weather_category, weather_description)
	SELECT weather_category, weather_description 
	FROM distinct_weather
	ORDER BY weather_category, weather_description;

-- create dim_traffic_type table
CREATE TABLE IF NOT EXISTS dim_traffic_type (
	dim_traffic_typeID SERIAL PRIMARY KEY,
	traffic_name VARCHAR
	);
TRUNCATE TABLE dim_traffic_type RESTART IDENTITY CASCADE;
WITH distinct_traffic AS (
	SELECT DISTINCT traffic_name
	FROM stage_traffic
	) INSERT INTO dim_traffic_type (traffic_name)
	SELECT traffic_name 
	FROM distinct_traffic
	ORDER BY traffic_name;
	
	
	
-- create fact_shared_mobility table
CREATE TABLE IF NOT EXISTS fact_shared_mobility (
	fact_shared_mobilityID SERIAL PRIMARY KEY,
	dim_timeperiodID INT REFERENCES dim_timeperiod(dim_timeperiodID),
	dim_locationID INT REFERENCES dim_location(dim_locationID),
	dim_providerID INT REFERENCES dim_provider(dim_providerID),
	dim_pickup_typeID INT REFERENCES dim_pickup_type(dim_pickup_typeID),
	dim_vehicle_typeID INT REFERENCES dim_vehicle_type(dim_vehicle_typeID),
	available_count INT
	);
TRUNCATE TABLE fact_shared_mobility RESTART IDENTITY CASCADE;
WITH data_keys AS (
	SELECT dtp.dim_timeperiodID, dl.dim_locationID, dp.dim_providerID, dpt.dim_pickup_typeID, dvt.dim_vehicle_typeID, sm.available_count FROM 
		stage_mobility sm
		LEFT OUTER JOIN dim_timeperiod dtp ON (sm.date_time = dtp.date_time)
		LEFT OUTER JOIN dim_location dl ON (sm.location = dl.location_name)
		LEFT OUTER JOIN dim_provider dp ON (sm.provider_name = dp.provider_name)
		LEFT OUTER JOIN	dim_pickup_type dpt ON (sm.pickup_type = dpt.pickup_type)
		LEFT OUTER JOIN dim_vehicle_type dvt ON (sm.vehicle_type = dvt.vehicle_type)
	) INSERT INTO fact_shared_mobility (dim_timeperiodID, dim_locationID, dim_providerID, dim_pickup_typeID, dim_vehicle_typeID, available_count)
	SELECT dim_timeperiodID, dim_locationID, dim_providerID, dim_pickup_typeID, dim_vehicle_typeID, available_count
	FROM data_keys
	ORDER BY dim_timeperiodID, dim_locationID, dim_providerID, dim_pickup_typeID, dim_vehicle_typeID;
	
	
	
	
-- create fact_weather table
CREATE TABLE IF NOT EXISTS fact_weather (
	fact_weatherID SERIAL PRIMARY KEY,
	dim_timeperiodID INT REFERENCES dim_timeperiod(dim_timeperiodID),
	dim_locationID INT REFERENCES dim_location(dim_locationID),
	dim_weather_conditionID INT REFERENCES dim_weather_condition(dim_weather_conditionID),
	temperature_celsius NUMERIC,
	visibility_percentage INT,
	windspeed_meters_per_second NUMERIC,
	cloudiness_percentage INT,
	rainfall_mm NUMERIC,
	snowfall_mm NUMERIC
	);
TRUNCATE TABLE fact_weather RESTART IDENTITY CASCADE;
WITH data_keys AS (
	SELECT dtp.dim_timeperiodID, dl.dim_locationID, dw.dim_weather_conditionID, sw.temperature_celsius, sw.visibility_percentage, sw.windspeed_meters_per_second, sw.cloudiness_percentage, sw.rainfall_mm, sw.snowfall_mm FROM 
		stage_weather sw
		LEFT OUTER JOIN dim_timeperiod dtp ON (sw.date_time = dtp.date_time)
		LEFT OUTER JOIN dim_location dl ON (sw.location = dl.location_name)
		LEFT OUTER JOIN dim_weather_condition dw ON (sw.weather_category = dw.weather_category AND sw.weather_description = dw.weather_description)
	) INSERT INTO fact_weather (dim_timeperiodID, dim_locationID, dim_weather_conditionID, temperature_celsius, visibility_percentage, windspeed_meters_per_second, cloudiness_percentage, rainfall_mm, snowfall_mm)
	SELECT dim_timeperiodID, dim_locationID, dim_weather_conditionID, temperature_celsius, visibility_percentage, windspeed_meters_per_second, cloudiness_percentage, rainfall_mm, snowfall_mm
	FROM data_keys
	ORDER BY dim_timeperiodID, dim_locationID, dim_weather_conditionID;


-- create fact_traffic table
CREATE TABLE IF NOT EXISTS fact_traffic (
	fact_trafficID SERIAL PRIMARY KEY,
	dim_timeperiodID INT REFERENCES dim_timeperiod(dim_timeperiodID),
	dim_locationID INT REFERENCES dim_location(dim_locationID),
	dim_traffic_typeID INT REFERENCES dim_traffic_type(dim_traffic_typeID),
	counter INT
	);
TRUNCATE TABLE fact_traffic RESTART IDENTITY CASCADE;
WITH data_keys AS (
	SELECT dtp.dim_timeperiodID, dl.dim_locationID, dt.dim_traffic_typeID, st.counter FROM 
		stage_traffic st
		LEFT OUTER JOIN dim_timeperiod dtp ON (st."DATUM" = dtp.date_time)
		LEFT OUTER JOIN dim_location dl ON (st.location = dl.location_name)
		LEFT OUTER JOIN dim_traffic_type dt ON (st.traffic_name = dt.traffic_name)
	) INSERT INTO fact_traffic (dim_timeperiodID, dim_locationID, dim_traffic_typeID, counter)
	SELECT dim_timeperiodID, dim_locationID, dim_traffic_typeID, counter
	FROM data_keys
	ORDER BY dim_timeperiodID, dim_locationID, dim_traffic_typeID;
	
	
-- drop stage tables
DROP TABLE stage_mobility;
DROP TABLE stage_traffic;
DROP TABLE stage_weather;