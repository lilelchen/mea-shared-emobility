# import packages

import logging
import os
import pandas as pd
import sqlalchemy
from datetime import datetime
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.hooks.postgres_hook import PostgresHook
from airflow.providers.postgres.operators.postgres import PostgresOperator
import boto3
import numpy as np
import ast
import io

FILEPATH = "dags/files"

# start function


def start():
    logging.info("Starting the DAG")
    logging.info(f"Path for files: {FILEPATH}")


# function to download mobility data to csv using pandas
def download_mobility():
    # establish hook
    db_hook = PostgresHook(
        postgres_conn_id="sharedmobility_datalake", schema="sharedmobility")
    # load required columns to pandas df
    df = db_hook.get_pandas_df("""
    SELECT 
        time_gmt, 
        location_name, 
        "attributes.provider.name", 
        "attributes.pickup_type", 
        "attributes.vehicle_type" , 
        "attributes.available", 
        "attributes.station.status.num_vehicle_available"
    FROM sharedmobility_mobility
    WHERE time_gmt >= '2022-03-30 22:00:00' AND time_gmt <= '2022-05-03 22:00:00';
    """)
    # save df
    df.to_csv(f"{FILEPATH}/sharedmobility_raw.csv")
    # write log
    logging.info("Data download successful")


# function to download weather data to csv using pandas
def download_weather():
    # establish hook
    db_hook = PostgresHook(
        postgres_conn_id="sharedmobility_datalake", schema="sharedmobility")
    # load all columns to pandas df
    df = db_hook.get_pandas_df("""
    SELECT * FROM sharedmobility_weather;
    """)
    # save df
    df.to_csv(f"{FILEPATH}/weather_raw.csv")
    # write log
    logging.info("Data download successful")


# function to download motorized vehicles, pedestrians and bicyclists data to csv using pandas
def download_s3():
    # establishing a connection fot both s3 buckets
    s3_bucket_name_mot = 'motorizedverhicles3'
    s3_bucket_name_ped_bicy = 'pedestrianbicycles3'
    # create client -every new session needs to be updated with the credentials below
    s3 = boto3.resource('s3',
                        aws_access_key_id='ASIAQK4FDUXM5J6YSLM4',
                        aws_secret_access_key='LbuM+B5swOTrxRUBFxMzLR3cDSBFoVPz0Egk/Heb',
                        aws_session_token='FwoGZXIvYXdzEIz//////////wEaDNGGJbmIHjeIVxdWuCK/AfyfGGYCDMuyMUMT8gzhv5rFjYAWBeauUNcngi7e+3lGE+thFBm3DNdIgSAKnGl5YOkrFQ9a0rPQBCg9uc5nswyGKtbFRQSoG7okMBL1RyYfacn/wuPzNaltiQEe762wXvUam4J2PYbjgHrpeu8HveyJFwHio8RtF0HzyG+Fjc4c+ht8Reg5w/HVtSYFnWkusAV61mZpk4uMVWUImsIibRERzwTaxULP1tU68m1pLV8FI5iH70U0rw/z2h7uq3JdKOywr5QGMi3C4d8KqEOnbj5zkQsVOHKNnakkhw8b+IpO88dhC1kLJzdJUAlMsi7f4x7oyqw=')
    # load from boto3
    motorized_response = s3.Bucket(s3_bucket_name_mot).Object(
        "motorizedverhicles3_zurich.csv").get()
    pedestrian_bicylists_response = s3.Bucket(s3_bucket_name_ped_bicy).Object(
        "pedestrianbicycles3_zurich.json").get()
    # load response body to DataFrame
    motorized_df_clean = pd.read_csv(io.BytesIO(
        motorized_response['Body'].read()), encoding='utf8')
    pedestrian_bicylists_df = pd.read_json(
        pedestrian_bicylists_response["Body"])
    pedestrian_bicylists_df_clean = pd.json_normalize(
        pedestrian_bicylists_df.result.records)
    # save df
    motorized_df_clean.to_csv(f"{FILEPATH}/motorized_raw.csv")
    pedestrian_bicylists_df_clean.to_csv(
        f"{FILEPATH}/pedestrian_bicylists_raw.csv")
    # write log
    logging.info("Data download from s3 successful")


# function to load mobility data from file and transform
def transform_mobility():
    # import csv to df
    df = pd.read_csv(f"{FILEPATH}/sharedmobility_raw.csv",
                     parse_dates=["time_gmt"],
                     index_col=0)
    # localize timezone
    df["date_time"] = df.time_gmt.dt.tz_localize(
        "GMT").dt.tz_convert("Europe/Zurich").dt.round("H").dt.tz_localize(None)
    # add proper count (for station based & free floating)
    df["available_count"] = [1 if (item == "free_floating" and available == True) else 0 if (
        item == "free_floating" and available == False) else station_count for item, station_count,
        available in zip(df["attributes.pickup_type"], df["attributes.station.status.num_vehicle_available"], df["attributes.available"])]
    # fix Lime & Publibike provider name
    df.replace("Lime City partners from Partners::RegionFeedMediator",
               "Lime", inplace=True)
    df.replace("Publibike",
               "PubliBike", inplace=True)
    # drop and rename columns
    df.rename(columns={
        "location_name": "location",
        "attributes.provider.name": "provider_name",
        "attributes.pickup_type": "pickup_type",
        "attributes.vehicle_type": "vehicle_type", }, inplace=True)
    df = df[["date_time", "location", "provider_name",
            "pickup_type", "vehicle_type", "available_count"]]
    # aggregate to atomic rows
    df = df.groupby(["date_time", "location", "provider_name", "pickup_type",
                     "vehicle_type"], as_index=False).sum()
    # save df
    df.to_csv(f"{FILEPATH}/sharedmobility_clean.csv")
    # write log
    logging.info("Data transformation successful")


# function to load weather data from file and transform
def transform_weather():
    # import csv to df
    df = pd.read_csv(f"{FILEPATH}/weather_raw.csv",
                     parse_dates=["time_gmt"],
                     index_col=0)
    # fix corrupted cells for weather discription data

    def fix_cell(cell):
        if cell.count("{") >= 2:
            cell = cell.split("},")
            cell = cell[0]
            cell = cell + "}]"
        else:
            pass
        return cell
    df["weather"] = df["weather"].apply(lambda x: fix_cell(x))
    # remove unnecessary starting and trailing characters from columns
    df["weather"] = df["weather"].apply(lambda x: x.strip("[]"))
    df["base"] = df["base"].apply(lambda x: x.strip('""'))
    df["name"] = df["name"].apply(lambda x: x.strip('""'))
    # transform from columns from type string to dictionary
    cols_to_be_transformed = ["coord", "weather",
                              "main", "wind", "clouds", "rain", "snow", "sys"]
    for column in cols_to_be_transformed:
        df[f"{column}"] = df[f"{column}"].apply(ast.literal_eval)
    # unnest dictionary cells into new columns
    cols_to_be_unnested = {
        "coord": ["lon", "lat"],
        "weather": ["id", "main", "description", "icon"],
        "main": ["temp", "feels_like", "temp_min", "temp_max", "pressure", "humidity"],
        "wind": ["speed", "deg", "gust"],
        "clouds": ["all"],
        "rain": ["1h"],
        "snow": ["1h"],
        "sys": ["type", "id", "country", "sunrise", "sunset"]}
    for key in cols_to_be_unnested.keys():
        for value in cols_to_be_unnested[f"{key}"]:
            df[f"{key}.{value}"] = df[f"{key}"].str[f"{value}"]
    # drop nested columns from df
    df.drop(cols_to_be_transformed, axis=1, inplace=True)
    # calculate localized time from gmt time
    df["time_local"] = df.time_gmt.dt.tz_localize("GMT").dt.tz_convert(
        "Europe/Zurich").dt.round("H").dt.tz_localize(None)
    # align location names and replace NaN values with '0'
    df["name"].replace({"Zürich (Kreis 6)": "Mainstation",
                        "Zürich (Kreis 1) / Lindenhof": "Mainstation",
                        "Zurich": "Mainstation",
                        "Zürich (Kreis 4) / Langstrasse": "Mainstation",
                        "Zürich (Kreis 1) / City": "Mainstation",
                        "Zürich (Kreis 11) / Oerlikon": "Oerlikon"}, inplace=True)
    df["rain.1h"].replace({np.NaN: 0}, inplace=True)
    df["snow.1h"].replace({np.NaN: 0}, inplace=True)
    df["wind.gust"].replace({np.NaN: 0}, inplace=True)
    # select columns relevant for analysis and save it into another dataframe
    selected_columns = df[["time_local", "name", "weather.main", "weather.description", "main.temp",
                           "visibility", "wind.speed", "clouds.all", "rain.1h", "snow.1h"]]
    df = selected_columns.copy()
    # rename column names
    df.rename(columns={"time_local": "date_time", "name": "location", "weather.main": "weather_category",
                       "weather.description": "weather_description", "main.temp": "temperature_celsius",
                       "wind.speed": "windspeed_meters_per_second", "clouds.all": "cloudiness_percentage",
                       "rain.1h": "rainfall_mm", "snow.1h": "snowfall_mm", "visibility": "visibility_percentage"},
              inplace=True)
    # delete duplicated rows
    df.drop_duplicates(subset=["date_time", "location"],
                       keep="first", inplace=True, ignore_index=True)
    # select data within specified time frame
    start_date = pd.date_range(start="2022-03-31 00:00:00",
                               end="2022-03-31 00:00:00", freq="H")
    end_date = pd.date_range(start="2022-05-03 23:00:00",
                             end="2022-05-03 23:00:00", freq="H")
    df = df.loc[df["date_time"] >= start_date[0]]
    df = df.loc[df["date_time"] <= end_date[0]]
    # save df
    df.to_csv(f"{FILEPATH}/weather_clean.csv")
    # write log
    logging.info("Data transformation successful")


# function to load motorized vehicles data from file and transform
def transform_motorized_vehicles():
    # import csv to df
    df_mot = pd.read_csv(f"{FILEPATH}/motorized_raw.csv",
                         index_col=0)
    # loading only the data collection stations within the two locations "Mainstation" and "Oerlikon"
    df_hb_mot = df_mot.loc[df_mot['ZSName'].isin(
        ['Gessnerallee (Usteribrücke)', 'Uraniastrasse (Bahnhofstrasse)', 'Bahnhofbrücke', 'Walchebrücke'])]
    df_oe_mot = df_mot.loc[df_mot['ZSName'].isin(
        ['Regensbergstrasse (Birchstrasse)', 'Binzmühlestrasse (Thurgauerstrasse)', 'Wehntalerstrasse (Zehntenhausplatz)'])]
    df_hb_mot['location'] = df_hb_mot['location'] = 'Mainstation'
    df_oe_mot['location'] = df_oe_mot['location'] = 'Oerlikon'
    # merge the two locations together
    df_mot_loc = df_hb_mot.append(df_oe_mot)
    # dropping the not relevant rows
    df_small_mot = df_mot_loc[['ZSName', 'Richtung', 'MessungDatZeit',
                               'AnzFahrzeuge', 'AnzFahrzeugeStatus', 'location']]
    # drop rows with "Fehlend" or "Imputiert"
    values = ['Fehlend', 'Imputiert']
    df_only_measured = df_small_mot[df_small_mot['AnzFahrzeugeStatus'].isin(
        values) == False]
    # building the mean out by "Location" and "ZSName"
    df_small_mot_mean = df_only_measured.groupby(
        ['ZSName', 'MessungDatZeit', 'location']).mean()
    # resetting the index
    df_resett_mot = df_small_mot_mean.reset_index()
    # building the mean out by "Location"
    df_mean_loc_mot = df_resett_mot.groupby(
        ['MessungDatZeit', 'location']).mean()
    # resetting the index
    df_mean_mot_resett = df_mean_loc_mot.reset_index()
    # changing datatypes
    df_mean_mot_resett['AnzFahrzeuge'] = pd.to_numeric(
        df_mean_mot_resett['AnzFahrzeuge'])
    df_mean_mot_resett['MessungDatZeit'] = pd.to_datetime(
        df_mean_mot_resett['MessungDatZeit'])
    # creating a new column
    df_mean_mot_resett['traffic_name'] = 'motorized_vehicle'
    # rename specific column names
    df_mean_mot_resett.rename(
        columns={'AnzFahrzeuge': 'counter'}, inplace=True)
    # save df
    df_mean_mot_resett.to_csv(f"{FILEPATH}/motorized_vehicles_clean.csv")
    # write log
    logging.info("Data transformation successful")


# function to load pedestrian and bicyclists data from file and transform
def transform_pedestrian_bicyclists():
    # import csv to df
    df_ped_bi = pd.read_csv(f"{FILEPATH}/pedestrian_bicylists_raw.csv",
                            index_col=0)
    # getting the relevant counting stations for the two locations
    df_hb_pb = df_ped_bi.loc[df_ped_bi['FK_ZAEHLER'].isin(
        ['Y2H19070283', 'Y2G13124876', 'U15G3063864', 'Y2G13124879'])]
    df_oe_pb = df_ped_bi.loc[df_ped_bi['FK_ZAEHLER'].isin(
        ['ECO10053914', 'ECO06040592', 'U15G3104445', 'U15G3104447', 'U15G3063866', 'U15G3063867', 'U15G3063865',
         'U15G3063866'])]
    # adding column for oerlikon and hb
    df_hb_pb['location'] = df_hb_pb['location'] = 'Mainstation'
    df_oe_pb['location'] = df_oe_pb['location'] = 'Oerlikon'
    # combine both dataframes
    df_pb_loc = df_hb_pb.append(df_oe_pb)
    # get nan when in all columns velo and fuss nan
    df_pb_loc.dropna(subset=['VELO_IN', 'VELO_OUT', 'FUSS_IN', 'FUSS_OUT'])
    # calculating mean of velo_in and velo_out
    df_pb_loc['velo'] = df_pb_loc[['VELO_IN', 'VELO_OUT']].mean(axis=1)
    # calculating mean of fuss_in and fuss_out
    df_pb_loc['fuss'] = df_pb_loc[['FUSS_IN', 'FUSS_OUT']].mean(axis=1)
    # only use the relevant columns
    df_pb_small = df_pb_loc[['FK_ZAEHLER',
                             'DATUM', 'velo', 'fuss', 'location']]
    # changing datum into datetime
    df_pb_small['DATUM'] = pd.to_datetime(df_pb_small['DATUM'])
    # taking the mean out of the multiple stations by location "Mainstation" or "Oerlikon"
    df_pb_small_mean = df_pb_small.groupby(['DATUM', 'location']).mean()
    # resetting the index
    df_pb_small_mean_resett = df_pb_small_mean.reset_index()
    # count "fuss" and "velo" per hour instead of quarter-hourly
    df_pb_hourly = df_pb_small_mean_resett.groupby([pd.Grouper(key='DATUM', freq='H'), 'location']).agg(
        {'velo': 'sum', 'fuss': 'sum'})
    df_pb_hourly_resetted = df_pb_hourly.reset_index()
    # creating two dataframes for fuss and velo
    df_velo = df_pb_hourly_resetted.filter(
        ['DATUM', 'location', 'velo'], axis=1)
    df_fuss = df_pb_hourly_resetted.filter(
        ['DATUM', 'location', 'fuss'], axis=1)
    # creating a new column
    df_velo['traffic_name'] = 'velo'
    df_fuss['traffic_name'] = 'fuss'
    # renaming existing column with count
    df_velo.rename({'velo': 'counter'}, axis='columns', inplace=True)
    df_fuss.rename({'fuss': 'counter'}, axis='columns', inplace=True)
    # merge both dataframes
    df_pb_hourly_resetted = df_velo.append(df_fuss, ignore_index=True)
    # save df
    df_pb_hourly_resetted.to_csv(f"{FILEPATH}/pedestrians_bicylists_clean.csv")
    # write log
    logging.info("Data transformation successful")


# function to combine motorized vehicles, pedestrians and bicyclists data from file
def combine_traffic():
    # import csv to df
    df_m_t = pd.read_csv(f"{FILEPATH}/motorized_vehicles_clean.csv",
                         parse_dates=["MessungDatZeit"],
                         index_col=0)
    df_pb_t = pd.read_csv(f"{FILEPATH}/pedestrians_bicylists_clean.csv",
                          parse_dates=["DATUM"],
                          index_col=0)
    df_m_t.rename(columns={'MessungDatZeit': 'DATUM'}, inplace=True)
    df_traffic = pd.merge(df_pb_t, df_m_t, how='outer')
    df_traffic = df_traffic.round(0)
    df_traffic['counter'] = df_traffic['counter'].astype(int)
    # changing datum into datetime
    df_traffic['DATUM'] = pd.to_datetime(df_traffic['DATUM'])
    df_traffic_date = df_traffic[~(
        df_traffic['DATUM'] < '2022-03-31 00:00:00')]
    df_traffic_date = df_traffic_date[~(
        df_traffic_date['DATUM'] > '2022-05-03 23:00:00')]
    # save df
    df_traffic_date.to_csv(f"{FILEPATH}/traffic_combined.csv")
    # write log
    logging.info("Data transformation successful")

# function to upload mobility data into data warehouse


def upload_mobility():
    # import csv to df
    df = pd.read_csv(f"{FILEPATH}/sharedmobility_clean.csv",
                     parse_dates=["date_time"],
                     index_col=0)
    # establish hook
    # TODO: adjust for required database type / hook
    db_hook = PostgresHook(
        postgres_conn_id="sharedmobility_warehouse", schema="sharedmobility_warehouse")
    db_eng = db_hook.get_sqlalchemy_engine()
    # upload data using pandas
    # TODO: index etc.
    df.to_sql(
        "stage_mobility",
        db_eng,
        if_exists="replace",
        index=False,
        method="multi",
        chunksize=10000
    )
    # write log
    logging.info("Data upload successful")


# function to upload weather data to data warehouse
def upload_weather():
    # import csv to df
    df = pd.read_csv(f"{FILEPATH}/weather_clean.csv",
                     parse_dates=["date_time"],
                     index_col=0)
    # establish hook
    # TODO: adjust for required database type / hook
    db_hook = PostgresHook(
        postgres_conn_id="sharedmobility_warehouse", schema="sharedmobility_warehouse")
    db_eng = db_hook.get_sqlalchemy_engine()
    # upload data using pandas
    # TODO: index etc.
    df.to_sql(
        "stage_weather",
        db_eng,
        if_exists="replace",
        index=False,
        method="multi",
        chunksize=10000
    )
    # write log
    logging.info("Data upload successful")


# function to upload traffic data into data warehouse
def upload_traffic():
    # import csv to df
    df = pd.read_csv(f"{FILEPATH}/traffic_combined.csv",
                     parse_dates=["DATUM"],
                     index_col=0)
    # establish hook
    # TODO: adjust for required database type / hook
    db_hook = PostgresHook(
        postgres_conn_id="sharedmobility_warehouse", schema="sharedmobility_warehouse")
    db_eng = db_hook.get_sqlalchemy_engine()
    # upload data using pandas
    # TODO: index etc.
    df.to_sql(
        "stage_traffic",
        db_eng,
        if_exists="replace",
        index=False,
        method="multi",
        chunksize=10000
    )
    # write log
    logging.info("Data upload successful")


# create DAG
with DAG(
    dag_id="mobility_weather_to_warehouse",
    description="ETL Pipeline for mobility and weather data from data lake to warehouse",
    schedule_interval="59 23 *  * 0",  # every sunday 32:59
    start_date=datetime(2022, 5, 1),
    catchup=False
) as dag:

    # start task
    t_start = PythonOperator(
        task_id="start_task",
        python_callable=start,
        dag=dag
    )

    # download mobility data to csv
    t_download_mobility = PythonOperator(
        task_id="download_mobility_data",
        python_callable=download_mobility,
        dag=dag
    )

    # download weather data to csv
    t_download_weather = PythonOperator(
        task_id="download_weather_data",
        python_callable=download_weather,
        dag=dag
    )

    # download s3 data to csv
    t_download_s3 = PythonOperator(
        task_id="download_s3_data",
        python_callable=download_s3,
        dag=dag
    )

    # transform mobility data (csv to csv)
    t_transform_mobility = PythonOperator(
        task_id="transform_mobility_data",
        python_callable=transform_mobility,
        dag=dag
    )

    # transform weather data (csv to csv)
    t_transform_weather = PythonOperator(
        task_id="transform_weather_data",
        python_callable=transform_weather,
        dag=dag
    )

    # transform motorized vehicles data (csv to csv)
    t_transform_motorized_vehicles = PythonOperator(
        task_id="transform_motorized_vehicles_data",
        python_callable=transform_motorized_vehicles,
        dag=dag
    )

    # transform pedestrians and bicyclists data (csv to csv)
    t_transform_pedestrians_bicyclists = PythonOperator(
        task_id="transform_pedestrians_bicyclists_data",
        python_callable=transform_pedestrian_bicyclists,
        dag=dag
    )

    # combine motorized vehicles and pedestrians, bicyclists data (csv to csv)
    t_combine_traffic = PythonOperator(
        task_id="combine_traffic_data",
        python_callable=combine_traffic,
        dag=dag
    )

    # upload mobility data to warehouse
    t_upload_mobility = PythonOperator(
        task_id="upload_mobility_data",
        python_callable=upload_mobility,
        dag=dag
    )

    # upload weather data to warehouse
    t_upload_weather = PythonOperator(
        task_id="upload_weather_data",
        python_callable=upload_weather,
        dag=dag
    )

    # upload traffic data to warehouse
    t_upload_traffic = PythonOperator(
        task_id="upload_traffic_data",
        python_callable=upload_traffic,
        dag=dag
    )

    # generate galaxy schema in postgres
    t_create_galaxy_schema = PostgresOperator(
        task_id="create_galaxy_schema",
        postgres_conn_id="sharedmobility_warehouse",
        sql="sql/generate_galaxy_schema.sql"
    )

# configure task dependencies
t_start >> t_download_mobility
t_start >> t_download_weather
t_start >> t_download_s3

t_download_mobility >> t_transform_mobility
t_download_weather >> t_transform_weather
t_download_s3 >> t_transform_motorized_vehicles
t_download_s3 >> t_transform_pedestrians_bicyclists

t_transform_motorized_vehicles >> t_combine_traffic
t_transform_pedestrians_bicyclists >> t_combine_traffic

t_transform_mobility >> t_upload_mobility
t_transform_weather >> t_upload_weather
t_combine_traffic >> t_upload_traffic

t_upload_mobility >> t_create_galaxy_schema
t_upload_traffic >> t_create_galaxy_schema
t_upload_weather >> t_create_galaxy_schema
