/* Integrity testing 
 * This integrity test helps to query the datawarehouse if the data is as expected after transformation.
*
* Authors: Manuel Frei, Jonas Klinkert, Anna-Lena Klaus
* Date: 09.06.2022
* 
* The following database was tested:
* - sharedmobility_warehouse (data warehouse)
* 
* Connect to sharedmobility_warehouse and run the following code*
*/

-- Thit SQL-query tests how many entries per table were made
-- In this table only two entries should be excepted (one for mainstation and one for Oerlikon)
select location_name from dim_location; -- should be 2

-- Counting the number of entries of providers
select COUNT (*) from dim_provider; --should be 8

-- checking the fact tables
select COUNT (*) from fact_shared_mobility; -- 16'084 expected
select COUNT (*) from fact_weather; -- 1'638 entries expected
select COUNT (*) from fact_traffic; -- 4'806 entries expected

-- Check for null values in specific column
SELECT *
FROM fact_shared_mobility
WHERE dim_pickup_typeid IS NULL;
-- result should be 0

-- Check for null values in specific column
SELECT *
FROM fact_traffic ft 
WHERE counter IS NULL;
-- result should be 0 results

-- Check if only six different vehicles types were given
select distinct
dvt.vehicle_type
from fact_shared_mobility fsm
inner join dim_vehicle_type dvt
on fsm.dim_vehicle_typeid = dvt.dim_vehicle_typeid 
;

-- Give max values from available_count to check if values are reasonable 
SELECT MAX(available_count)
FROM fact_shared_mobility
;

-- Give min values from available_count to check if values are reasonable 
SELECT MIN(available_count)
FROM fact_shared_mobility
;

-- Give values between specific availability range where it snowed
select 
fs.available_count,
fs.fact_shared_mobilityid,
dwc.weather_category 
from fact_shared_mobility fs
inner join dim_timeperiod dtp
on dtp.dim_timeperiodid = fs.dim_timeperiodid
inner join fact_weather fw
on fw.dim_timeperiodid = dtp.dim_timeperiodid 
inner join dim_weather_condition dwc 
on dwc.dim_weather_conditionid = fw.dim_weather_conditionid 
where (available_count between 50::int and 100::int)
and dwc.weather_category = 'Snow'
order by fs.fact_shared_mobilityid DESC;
-- gives 98 entries with highest fact_shared_mobilityid of 4834

-- The dataset containts data from weekends
select count(*)
from dim_timeperiod
where weekend is true
-- results is 240 for this specific time period
;


-- Checking on how many hours it rained
select count(*)
from fact_weather fw
inner join dim_timeperiod dtp
on dtp.dim_timeperiodid = fw.dim_timeperiodid
inner join dim_weather_condition dw
on dw.dim_weather_conditionid  = fw.dim_weather_conditionid
where fw.rainfall_mm::int > 0::int
-- it rained on 70 hours during the whole timeframe (=more than 0 mm)
;


