# Availability of Shared Mobility Services in the City of Zurich

This project is part of the modules "Data Warehouse and Data Lake 1 + 2" within the program MSc. Applied Information and Data Science at the Lucerne University of Applied Sciences and Arts (HSLU). 

**Project team members:**
* Anna-Lena Klaus
* Manuel Frei
* Jonas Klinkert

This repository contains all documents and files related to the project. 

## Project Description

The goal of the project is the examination of the availability of shared mobility services based on temporal, spatial and external dependencies and the detection and description of usage behaviour and pattern.

## Repository Structure

The repository is structured as follows:

```
├── .ipynb_checkpoints
├── Final Reports
    ├── DWL1_Team_KlFrKl_final_report.pdf
    └── DWL2_Team_KlFrKl_final_report.pdf
├── SQL_queries
    └── integrity_tests.sql
├── airflow
│   ├── .idea
│   ├── dags
        ├── files
        ├── sql
            └── generate_galaxy_schema.sql
        └── mobility_to_warehouse.py
    ├── logs
    ├── plugins
    └── docker-compose.yaml
├── lambda_functions
│   ├── .ipynb_checkpoints
│   ├── sharedmobility_lambda_pedestrians_bicycles.py
│   ├── sharedmobility_lambda_motorized_vehicles.py
│   ├── requirements.txt
│   ├── sharedmobility_lambda_mobility.py
│   └── sharedmobility_lambda_weather.py
├── .env
├── .gitignore
├── README.md
├── data_exploration.ipynb
```

## Getting started

Starting to work with the project, read access is possible for everyone.
To use the code to built up your own pipeline, copy the code of the lambda_functions into your AWS lambda service. Successfully running the data_exploration file is only possible, if you built up the data pipeline on AWS.
Further details are included in the documentation "Team_KlFrKl_final_report.docx" which is available on demand (annalena.klaus"*_at_*"stud.hslu.ch).

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/lilelchen/mea-shared-emobility.git
git branch -M main
git push -uf origin main
```
